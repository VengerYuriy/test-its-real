import BracesCounter from '../BracesCounter';
import RandomComponents from '../RandomTasks';
import './style.css';

function App() {
  return (
    <div className="App">
        <BracesCounter/>
        <RandomComponents/>
    </div>
  );
}

export default App;
