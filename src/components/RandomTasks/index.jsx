import { useState, useEffect } from 'react'
import './style.css'

const RandomTasks = () => {

    const [tasks, setTasks] = useState([]);
    let timer

    const updateTask = (tasks, currentDate) => {
        tasks.forEach(task => {
            task.value = `Исчезнет через ${Math.ceil((task.endDate - currentDate)/1000)} секунд`
        })
    }

    const newTask = (currentDate) => {
        const taskLifeTime = Math.floor(Math.random()*20 + 10),
              taskEndDate = new Date (currentDate.setSeconds(currentDate.getSeconds() + taskLifeTime));

        const task = {
            endDate: taskEndDate
        }

        const tasksCopy = [...tasks, task]

        updateTask(tasksCopy, new Date())
        setTasks(tasksCopy) 
    }

    const checkLifeTime = () => {
        timer = setInterval(()=> {
            const actualTasks = []

            tasks.forEach(task => {
                new Date() < task.endDate && actualTasks.push(task)
            })
            
            if (actualTasks.length !== tasks.length) {
                updateTask(actualTasks, new Date())
                setTasks(actualTasks)
            }
        }, 1000)
    }

    useEffect(()=> {
        tasks.length && checkLifeTime()
        return () => {
            clearInterval(timer)
        }
        // eslint-disable-next-line
    },[tasks])

    return (
        <div className="random-tasks">
            <h1>Task 2</h1>
            <button onClick={()=> newTask(new Date())} className='random-tasks__add-button'>add task</button>
            <div className="random-components__tasks">
                {tasks.map((item, index) => 
                    <p key={index} className='random-components__task'>{index+1}. {item.value}</p>
                )}
            </div>
        </div>
    )
}

export default RandomTasks