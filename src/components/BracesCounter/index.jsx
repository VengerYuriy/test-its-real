import { useState } from "react"
import './style.css'

const BracesCounter = () => {

    const [inputValue, setInputValue] = useState(''),
          [correctBracesAmount, setCorrectBracesAmount] =useState(0),
          [incorrectBracesAmount, setIncorrectBracesAmount] =useState(0)

    const checkBraces = (str) => {

        const bracesTable = {
            "(" : ")",
            "{" : "}",
            "[" : "]",
            openBraces: ["(", "{", "["],
            closeBraces: [")", "}", "]"]
        }

        const symbolArray = str.split('')

        const correctBraces = [],
              incorrectBraces = [],
              currentOpenBraces = []

        symbolArray.forEach((currentSymbol)=> {
            const lastOpenBrace = currentOpenBraces[currentOpenBraces.length-1]

            if (bracesTable.openBraces.indexOf(currentSymbol) >= 0) {
                currentOpenBraces.push(currentSymbol)
            } else if (bracesTable.closeBraces.indexOf(currentSymbol) >= 0) {
                if (currentOpenBraces.length === 0) {
                    incorrectBraces.push(currentSymbol)
                } else {
                    bracesTable[lastOpenBrace] === currentSymbol ? correctBraces.push(lastOpenBrace, currentSymbol) : incorrectBraces.push(lastOpenBrace    , currentSymbol)
                    currentOpenBraces.splice(currentOpenBraces.length-1, 1)
                }
            }
        })

        if (currentOpenBraces.length > 0) {
            incorrectBraces.push(...currentOpenBraces)
        }

        setCorrectBracesAmount(correctBraces.length)
        setIncorrectBracesAmount(incorrectBraces.length)

    }

    return (
        <div className="braces-counter">
            <h1>Task 1</h1>
            <input type="text" placeholder="input string" onChange={(e)=> setInputValue(e.target.value)}/>
            <button onClick={()=> checkBraces(inputValue)}>check string</button>
                <p className="braces-counter__value">{`Кол-во корректно введеных скобок: ${correctBracesAmount}`}</p>
                <p className="braces-counter__value">{`Кол-во некорректно введеных скобок: ${incorrectBracesAmount}`}</p>
        </div>
    )
}

export default BracesCounter